const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");

const port = process.env.PORT || 4000;

const app = express();

mongoose.connect("mongodb+srv://Atanpogi:admin123@batch204-nuestrojonatha.vsomfie.mongodb.net/capstone-3?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});



let db = mongoose.connection;
db.on("error", () => console.error.bind(console, "Error"));
db.once("open", () => console.log("Naka kabit na sa MongoDB Atlas"))

app.use(cors());
app.use(express.json());

app.use("/user", userRoutes);
app.use("/product", productRoutes);
app.use("/order", orderRoutes);




app.listen(port, ()=> {
	console.log(`ang API ay nakikinig sa ika-${port} na port`)
});