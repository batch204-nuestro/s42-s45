const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController");
const auth = require("../auth");

//Create an order
router.post("/pabili", auth.verify, (req, res) => {


	let data = {
		// User ID and IsAdmin will be retrieved from the request header
		userId: auth.decode(req.headers.authorization).id,
		// Course ID will be retrieved from the request body
		products: req.body.products,
		totalAmount: req.body.totalAmount,
		isAdmin: auth.decode(req.headers.authorization).isAdmin

	};
	

console.log(data);
	orderController.pabili(data).then(resultFromController => res.send(resultFromController));

});


//Retrieve user deatils and  history
router.get("/history", (req, res) => {
	let name = {
		userId: auth.decode(req.headers.authorization).id
	}

console.log(name)
orderController.getAllOrder(name).then(resultFromController => res.send(resultFromController));

});



//retrieve authenticated user history
router.get("/cart", (req, res) => {
	let name = {
		userId: auth.decode(req.headers.authorization).id
	}

console.log(name)
orderController.getCart(name).then(resultFromController => res.send(resultFromController));

});




module.exports = router;
