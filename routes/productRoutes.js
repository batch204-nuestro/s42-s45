const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require('../auth.js');

//Route for creating a Product
router.post("/create", auth.verify, (req, res) => {

	const isAdmin = auth.decode(req.headers.authorization).isAdmin
	console.log(isAdmin);

	if(isAdmin) {
		productController.addProduct(req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}
	

});


//Route for retrieving all the ACTIVE Product
router.get("/", (req, res) => {

	productController.getAllActive().then(resultFromController => res.send(resultFromController));

});

//Route for retrieving all the courses
router.get("/all", (req, res) => {

	productController.getAllProducts().then(resultFromController => res.send(resultFromController))

});




//REtrieve single product
router.get("/:productId", (req, res) => {

	console.log(req.params)

productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));

});


//Route for Updating a Product
// JWT verification is needed for this route to ensure that a user is logged in before updating a course
router.put("/:productId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
console.log(userData)
console.log(req.params)
	productController.updateProduct(req.params, req.body, userData).then(resultFromController => res.send(resultFromController));

});


//Archive product
router.put('/:productId/archive', auth.verify, (req, res) => {

	console.log(req.params)

	const data = {
		productId : req.params.productId,
		payload : auth.decode(req.headers.authorization).isAdmin
	}
console.log(data)
	productController.archiveProduct(data, req.body).then(resultFromController => res.send(resultFromController))
});


//Enable product
router.put('/:productId/restore', auth.verify, (req, res) => {

	console.log(req.params)

	const data = {
		productId : req.params.productId,
		payload : auth.decode(req.headers.authorization).isAdmin
	}
console.log(data)
	productController.restoreProduct(data, req.body).then(resultFromController => res.send(resultFromController))
});




module.exports = router;