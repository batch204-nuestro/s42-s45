const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order")
const bcrypt = require("bcrypt");
const auth = require("../Auth");


module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then(result => {


		if(result.length > 0) {
			return true
		} else {
			return false
		}

	})
}


//Controller for User Registration
module.exports.registerUser = (reqBody) => {

	let newUser = new User({

		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		//bcrypt.hashSync(<dataTobeHash>, <saltRound>)
		// 10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
		password: bcrypt.hashSync(reqBody.password, 10)
	});


	return newUser.save().then((user, error) => {

		if(error) {
			return false
		} else {
			return true
		}

	})
}


// //Controller for User Registration
// module.exports.registerUser = (reqBody) => {
// 	return User.findOne({email: reqBody.email}).then(result => {

// 		if(result !== null) {
// 			return false
// 		} else {


// 		let newUser = new User({
// 				firstName: reqBody.firstName,
// 				lastName: reqBody.lastName,
// 				email: reqBody.email,
// 				mobileNo: reqBody.mobileNo,
// 				password: bcrypt.hashSync(reqBody.password, 10),
// 				isAdmin: reqBody.isAdmin

// 		});

// 		return newUser.save().then((user, error) => {

// 			if(error) {
// 				return false
// 			} else {
// 				return true
// 			}

// 		})
// 	}
// 	})
// }

//User login
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if(result == null) {
			
			return false

		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect) {

				console.log(result)

				return { access: auth.createAccessToken(result)}

				
			} else {

				return false
			}
		}

	})
}


module.exports.getProfile = (data) => {

	return User.findById(data.id).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;

	});

};

