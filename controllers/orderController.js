const Product = require("../models/Product");
const User = require("../models/User");
const Order = require("../models/Order")
const bcrypt = require("bcrypt");
const auth = require("../auth");


//Create an order
module.exports.pabili = (data) => {

	if (data.isAdmin === true) {
		return false
	} else {

		let newOrder = new Order({
			userId: data.userId,
			products: data.products,
			totalAmount: data.totalAmount


		});


			return newOrder.save().then( (order, error) => {

				if(error) {
					return false
				} else {
					return true
				
				}
			})

		}

}


//REtrieve user details
module.exports.getAllOrder = async (name) => {

	
	let findUser = await User.findById(name.userId);

	let findOrder = await Order.find({userId: name.userId});


	return [findUser, findOrder]

}


//Retrive user cart

module.exports.getCart =  (name) => {

	
	// let findUser = await User.findById(name.userId);

	let findOrder = Order.find({userId: name.userId});


	return	(findOrder)

}