const Product = require("../models/Product");
const User = require("../models/User");
const Order = require("../models/Order")

module.exports.addProduct = (reqBody) => {

	return Product.findOne({name: reqBody.name}).then(result => {

		if(result !== null) {
			return false
		} else {

			let newProduct = new Product({

				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price
			});


			// Saves the created object to our database
			return newProduct.save().then( (product, error) => {

				// Course creation successful
				if(error) {

					return false
				
				// Course creation failed
				} else {

					return true
				}


			})
		}
	})
}



//Controller for retrieving all ACTIVE courses
module.exports.getAllActive = () => {

	return Product.find({isActive: true}).then( result => {
		return result;
	})

}

//Retrieve all courses
module.exports.getAllProducts = () => {

	return Product.find({}).then(result => {

		return result;
	
	})

}



//Retrieving a single product
module.exports.getProduct = (reqParams) => {


	return Product.findById(reqParams.productId).then(result => {
		
		return result
	
	})

}


//Updating Product
module.exports.updateProduct = (reqParams, reqBody, userdata) => {

	return User.findById(userdata.id).then(result => {
		console.log(result)

		if(result.isAdmin === true) {

			// Specify the fields/properties of the document to be updated
			let updatedProduct = {
				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price
			};

		
			return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {

			
				if(error) {

					return false

				// Course updated successfully	
				} else {

					return true
				}
			})

		} else {

			return false
		}

	})


}



//ARchiving Product



module.exports.archiveProduct = (data) => {

	return Product.findById(data.productId).then((result, err) => {

		if(data.payload === true) {

			result.isActive = false;

			return result.save().then((archivedProduct, err) => {

				// Product not archived
				if(err) {

					return false;

				// Product archived successfully
				} else {

					return true;
				}
			})

		} else {

			//If user is not Admin
			return false
		}

	})
}



//Restore Product

module.exports.restoreProduct = (data) => {

	return Product.findById(data.productId).then((result, err) => {

		if(data.payload === true) {

			result.isActive = true;

			return result.save().then((restoredProduct, err) => {

				// Product not archived
				if(err) {

					return false;

				// Product archived successfully
				} else {

					return true;
				}
			})

		} else {

			//If user is not Admin
			return false
		}

	})
}